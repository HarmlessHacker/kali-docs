---
title: Installing NetHunter On the TicWatch Pro 3
description:
icon:
weight:
author: ["yesimxev","HarmlessHacker"]
---

![](NetHunter-TicWatchPro3.png)

All variants are supported (TicWatch Pro 3 GPS/LTE/Ultra GPS/Ultra LTE) with a generic installer as of now.

# From unpacking to running NetHunter in 5 steps:

1. Unlock the bootloader
2. Flash TWRP, WearOS image, Magisk, dm-verity disabler
3. Finalise Magisk app to finish the rooting process
4. Install NetHunter
5. Set NetHunter watch face 

## 1. Unlock the bootloader

- Connect your watch to your PC with a [DIY USB cable](https://wear.lunawolf.es/en/crear-custom-cable) or a [3D printed data dock](https://social.thangs.com/m/59021), and fire up a terminal.
- Download [adb platform tools](https://developer.android.com/studio/releases/platform-tools)
- If you have set up your watch on the phone you can access settings, otherwise hold both buttons for a few seconds on the welcome screen
- Enable developer settings by going to System -> About -> tap Build number 10 times
- Enable ADB, re-plug USB and accept debug from PC
- Reboot into bootloader with `adb reboot bootloader` from the terminal
- Unlock bootloader with `fastboot oem unlock` 

*Note*: If you have mac or linux or already had platform tools please make sure it is version: 30.0.5 so that the --disable-verity command works correctly.


## 2. Flash TWRP, WearOS image, Magisk, dm-verity disabler

- Again enable ADB, and reboot to bootloader with `adb reboot bootloader`
- Disable vbmeta verification: `fastboot --disable-verity --disable-verification flash vbmeta vbmeta.img`
- Flash recovery `fastboot flash recovery rubyfish_recovery.img`
- Boot into TWRP recovery using `fastboot reboot recovery` or by selecting it with the side buttons (switch options with bottom one, select option with upper button)
- From TWRP **MAKE A BACKUP** of every partition and transfer them to your computer.
- From your computer, make a copy of your Magisk apk file and change the extension from apk to Magisk-v24.3.zip. You should have one of each.
- Transfer all required .zip & .apk files from the "downloads" section of this tutorial to the smartwatch using `adb push`. 
No wipe of any kind is required before installing, the OneOS install itself does the necessary wipes except the "data" partition
- Flash `OneOS-XX.XX.XX_rubyfish.zip` from TWRP by installing as zip
- Flash Mobvoi Apps, Magisk & Disabler zip files (in that order) using the same zip install method via TWRP
- Wipe the data partition from TWRP itself going to `wipe` --> `advanced wipe` --> select `data`, then swipe to erase the `data` partition
- Reboot & do initial setup (pair with your phone through WearOS app)

*Note 2*: When you install magisk, it will be necessary to wipe the device `data` partition, otherwise the watch will not start

## 3. Finalise Magisk app to finish the rooting process
- Reboot to recovery with `adb reboot recovery`
- Finalize Magisk installation from TWRP by installing `Magisk-v24.3.apk` as zip, like in step 2 (make sure you are flashing the *apk* file)
- Launch Magisk Manager & Follow Prompts
- You might want to disable auto-update, set grant access in auto response, and disable toast notifications for easier navigation in the future

## 4. Install NetHunter

- Reboot to recovery with `adb reboot recovery`
- Flash NetHunter image with TWRP Install -> ZIP
- Reboot (if it doesn't boot, you might need to wipe your `data` partition again)
- Start NetHunter app & chroot (Grant SU permissions if asked)

## 5. Set NetHunter watch face

- Install Facer onto your phone and watch from Play Store 
- Search for NetHunter
- Select & Sync (from watch, deny location and sensor permissions for app)

### Enjoy Kali NetHunter on the TicWatch Pro 3

## Downloads

- [Magisk for Wear OS](https://wear.lunawolf.es/en/magisk-wear-os)
- [TWRP Recovery image](https://wear.lunawolf.es/en/descargar)
- [OneOS ROM and Mobvoi package](https://wear.lunawolf.es/en/descargar) (select "Custom ROM" tab)
- [vbmeta image](https://wear.lunawolf.es/en/descargar) (select "Utilities" tab)
- [dm-verity disabler](https://build.nethunter.com/contributors/re4son/guacamole/Disable_Dm-Verity_ForceEncrypt_11.02.2020.zip)
- [WearOS NetHunter zip](https://build.nethunter.com/contributors/re4son/catfish/nethunter-2022.2b-generic-armhf-kalifs-nano.zip)

## Additional recommended apps

- TotalCommander: useful for selecting eg. a Ducky script, use "adb install" method
Download link: https://www.totalcommander.ch/android/tcandroid331-armeabi.apk

## Supported features

- Kali services
- Custom Commands
- MAC Changer
- HID Attacks
- DuckHunter
- Nmap Scan
- WPS Attacks

## Upcoming features (not guaranteed)

- Nexmon, as the chipset is supported, needs some time
- Bluetooth Arsenal (internal bluetooth via blueblinder, as carwhisperer fails to r/w when SCO channel is connected)
- Router Keygen (to be optimised)
- Hijacker (if nexmon succeeds)
- Mifare Classic Tool (need to build OS with android.hardware.nfc enabled)

## Hardware limitations

- Power resource is not enough for any external adapters, although this kernel might support Y cable in the future!

